# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

find_program(png2icns_EXE png2icns)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(png2icns
    FOUND_VAR
        png2icns_FOUND
    REQUIRED_VARS
        png2icns_EXE
)

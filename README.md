<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
-->

Handy dandy tool to generate `/.VolumeIcon.{png,icns}` via a systemd unit.

VolumeIcon is primarily consumed by the EFI bootloader Refind :https://rodsbooks.com/refind/configfile.html

The icon is based on whatever LOGO is defined in /etc/os-release and resolved
from the hicolor icon set. At the very least a 256px resolution of the logo should
be available.

The PNG is always created, the icns file meanwhile is dependent on libicns's png2icns tool at runtime.
If the tool is missing the icns does not get generated.

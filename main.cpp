// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

#include <KIconEngine>
#include <KIconLoader>
#include <KOSRelease>
#include <QDebug>
#include <QFile>
#include <QGuiApplication>
#include <QIcon>
#include <QPixmap>
#include <QProcess>
#include <QStandardPaths>

static QString scaleSuffix(int scale)
{
    if (scale == 1) {
        return {};
    }
    return QStringLiteral("@%1x").arg(scale);
}

static QString store(const QIcon &i, int size, int scale)
{
    const QString suffix = scaleSuffix(scale);
    const QString name = QStringLiteral("icon_%1x%1%2.png").arg(QString::number(size), suffix);
    const auto p = i.pixmap(size * scale);
    Q_ASSERT(!p.isNull());
    const auto ret = p.save(name);
    Q_ASSERT(ret);
    return name;
}

static QString writeICNS(const QIcon &icon)
{
    const QString png2icns = QStandardPaths::findExecutable("png2icns");
    if (png2icns.isEmpty()) {
        qWarning() << "png2icns not found";
        return {};
    }

    // libicns is a bit wonky, it doesn't correctly support hidpi scales and we are forced to render everything with
    // scale 1. Within that scale we exhaust all resolutions it supports though.
    // That is as of 0.8.1 in ubuntu.
    // Should this change in the future it may still require some tinkering since resolutions are tagged uniquely
    // in the binary blobs so e.g. 16@2 is 32x32 but that is also 32@1  which png2icns refuses to deal with.
    const std::initializer_list<uint> sizes = {16, 32, 128, 256, 512, 1024};
    const std::initializer_list<uint> scales = {1};

    const auto icns = QStringLiteral(".VolumeIcon.icns");
    QStringList files{icns};
    files.reserve(1 /* icns arg */ + (sizes.size() * scales.size()));

    for (const auto &scale : scales) {
        for (const auto &size : sizes) {
            // After some soul searching I've concluded that we'll simply accept whatever QIcon gives us.
            // There certainly is an option for more in-depth selection of what sizes may be appropriate or not, but
            // in the end it is a zero sum game. If a 512 icon is needed but we only packed 256 then the UI
            // displaying needs to scale up the icon anyway. At that point we may as well do the scaling and provide a
            // shitty 512 that was scaled up.
            const QString path = store(icon, size, scale);
            files.push_back(path);
        }
    }

    QProcess proc;
    proc.setProcessChannelMode(QProcess::ForwardedChannels);
    proc.start(QStringLiteral("png2icns"), files);
    Q_ASSERT(proc.waitForFinished());
    Q_ASSERT(proc.exitCode() == 0);
    return icns;
}

static QString writePNG(const QIcon &icon)
{
    const auto path = QStringLiteral(".VolumeIcon.png");
    // Completely arbitrary size that should usually be available for an icon.
    const auto pixmap = icon.pixmap(256);
    if (pixmap.isNull()) {
        return {};
    }
    if (!pixmap.save(path)) {
        qWarning() << "Failed to save" << path;
        return {};
    }
    return path;
}

int main(int argc, char **argv)
{
    // We need a gui to use qpixmap but we'll only use the minimal platform since we don't actually need to
    // do anything but pixmap manipulation.
    setenv("QT_QPA_PLATFORM", "minimal", 1);
    QGuiApplication app(argc, argv);

    const QString logo = KOSRelease().logo();
    if (logo.isEmpty()) {
        return 0;
    }

    // Manually create a KIconEngine, with the minimal enviornment we are run in there's not going to be much
    // in teh way of platform plugins and the ike.
    QIcon icon(new KIconEngine(logo, KIconLoader::global()));
    if (icon.isNull()) {
        qWarning() << "NO ICON";
    }
    Q_ASSERT(!icon.isNull());

    const auto cache = QStringLiteral("/.VolumeIconCache.key");
    if (QFile cacheFile(cache); cacheFile.open(QFile::ReadWrite)) {
        bool ok = false;
        if (icon.cacheKey() == cacheFile.readAll().simplified().toLongLong(&ok)) {
            qDebug() << "Cache key unchanged" << icon.cacheKey();
            return 0;
        }
        if (!ok) {
            qWarning() << "Failed to parse cache value as longlong!";
        }
        cacheFile.resize(0); // truncate
        const bool written = cacheFile.write(QByteArray::number(icon.cacheKey()));
        qDebug() << "cache file updated?" << written;
    } else {
        qWarning() << "Failed to open cache file";
    }

    QStringList artifacts{writePNG(icon), writeICNS(icon)};
    artifacts.removeAll(QString());

    bool fail = false;
    const auto root = QStringLiteral("/");
    for (const auto &file : artifacts) {
        const auto path = root + file;
        qDebug() << "moving " << file << "->" << path;
        QFile::remove(path);
        const bool renamed = QFile::rename(file, path);
        if (!renamed) {
            qWarning() << "  Failed!";
            fail = true;
        }
    }

    return fail ? 1 : 0;
}
